#!/usr/bin/awk -f

# scrippies/gsuite-users-mutt-aliases.awk

# read google g suite user download csv; write mutt aliases.

BEGIN {
  FS=",";
  _assert_exit = 0;
}

# Robbins, Arnold; (2015-02-27). _Effective AWK
# Programming_. O'Reilly. pp. 245-246. ISBN 9781491904619
function assert(condition, string) {
  if (! condition) {
    printf("%s:%d: assertion failed: %s\n",
           FILENAME, FNR, string) >"/dev/stderr";
    _assert_exit = 1;
    exit 1;
  }
}

END {
  if (_assert_exit) {
    exit 1;
  }
}

# take a string, return the given string stripped of any/all leading and/or
# trailing whitespace
function trim(x) {
  gsub(/^[[:space:]]+/,"",x);
  gsub(/[[:space:]]+$/,"",x);
  return x;
}

# take a string; return the given string stripped of any/all non-alphanumeric
# characters, in lowercase
function loalnum(x) {
  gsub(/[^[:alnum:]]+/,"",x);
  return tolower(x);
}

# take first name, last name, email address, nickname suffix; return a
# mutt-compatible alias
#
# https://gitlab.com/muttmua/mutt/-/wikis/MuttGuide/Aliases
function alias_suffix(first, last, email, suffix) {
  first = trim(first);
  last = trim(last);
  return sprintf("alias %s.%s%s %s %s <%s>",
                 loalnum(first), loalnum(last), suffix, first, last, tolower(email));
}

# take first name, last name, email address; return a mutt-compatible alias
#
# https://gitlab.com/muttmua/mutt/-/wikis/MuttGuide/Aliases
function alias(first, last, email) {
  return alias_suffix(first, last, email, "");
}

{
  # The csv that google generates has DOS-style "\r\n" line
  # terminators. Proactively remove all "\r" characters from every line.
  gsub(/\r/,"");

  if (FNR == 1) {
    # This is the first line of the file. Assert that the column headings are
    # what we expect.
    assert($1 == "First Name [Required]", "$1 == First Name [Required]");
    assert($2 == "Last Name [Required]", "$2 == Last Name [Required]");
    assert($3 == "Email Address [Required]", "$3 == Email Address [Required]");
    assert($4 == "Password [Required]", "$4 == Password [Required]");
    assert($5 == "Password Hash Function [UPLOAD ONLY]", "$5 == Password Hash Function [UPLOAD ONLY]");
    assert($6 == "Org Unit Path [Required]", "$6 == Org Unit Path [Required]");
    assert($7 == "New Primary Email [UPLOAD ONLY]", "$7 == New Primary Email [UPLOAD ONLY]");
    assert($8 == "Status [READ ONLY]", "$8 == Status [READ ONLY]");
    assert($9 == "Last Sign In [READ ONLY]", "$9 == Last Sign In [READ ONLY]");
    assert($10 == "Recovery Email", "$10 == Recovery Email");
    assert($11 == "Home Secondary Email", "$11 == Home Secondary Email");
    assert($12 == "Work Secondary Email", "$12 == Work Secondary Email");
    assert($13 == "Recovery Phone [MUST BE IN THE E.164 FORMAT]", "$13 == Recovery Phone [MUST BE IN THE E.164 FORMAT]");
    assert($14 == "Work Phone", "$14 == Work Phone");
    assert($15 == "Home Phone", "$15 == Home Phone");
    assert($16 == "Mobile Phone", "$16 == Mobile Phone");
    assert($17 == "Work Address", "$17 == Work Address");
    assert($18 == "Home Address", "$18 == Home Address");
    assert($19 == "Employee ID", "$19 == Employee ID");
    assert($20 == "Employee Type", "$20 == Employee Type");
    assert($21 == "Employee Title", "$21 == Employee Title");
    assert($22 == "Manager Email", "$22 == Manager Email");
    assert($23 == "Department", "$23 == Department");
    assert($24 == "Cost Center", "$24 == Cost Center");
    assert($25 == "2sv Enrolled [READ ONLY]", "$25 == 2sv Enrolled [READ ONLY]");
    assert($26 == "2sv Enforced [READ ONLY]", "$26 == 2sv Enforced [READ ONLY]");
    assert($27 == "Building ID", "$27 == Building ID");
    assert($28 == "Floor Name", "$28 == Floor Name");
    assert($29 == "Floor Section", "$29 == Floor Section");
    assert($30 == "Email Usage [READ ONLY]", "$30 == Email Usage [READ ONLY]");
    assert($31 == "Drive Usage [READ ONLY]", "$31 == Drive Usage [READ ONLY]");
    assert($32 == "Photos Usage [READ ONLY]", "$32 == Photos Usage [READ ONLY]");
    assert($33 == "Storage limit [READ ONLY]", "$33 == Storage limit [READ ONLY]");
    assert($34 == "Storage Used [READ ONLY]", "$34 == Storage Used [READ ONLY]");
    assert($35 == "Change Password at Next Sign-In", "$35 == Change Password at Next Sign-In");
    assert($36 == "New Status [UPLOAD ONLY]", "$36 == New Status [UPLOAD ONLY]");
    assert($37 == "Licenses [READ ONLY]", "$37 == Licenses [READ ONLY]");
    assert($38 == "New Licenses [UPLOAD ONLY]", "$38 == New Licenses [UPLOAD ONLY]");

    # Getting to here implies that all 38 column headings are what we
    # expect. Skip to the next line.
    next;
  }

  if (false) {
    # Maybe do this someday if and when I need additional aliases for the other
    # email addresses.
    if ($12) {
      # Work Secondary Email
      print alias_suffix($1,$2,$12,".work");
    }
    if ($11) {
      # Home Secondary Email
      print alias_suffix($1,$2,$11,".home");
    }
    if ($10) {
      # Recovery Email
      print alias_suffix($1,$2,$10,".recovery");
    }
  }
  # print the regular alias
  print alias($1,$2,$3);
}
