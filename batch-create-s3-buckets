#!/bin/sh
# shellcheck disable=SC2317
# scrippies/batch-create-s3-buckets

set -eu

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

cleanup() {
    status="$?"
    return "${status}"
}

usage() {
    cat <<EOF
Usage: $0 [ARGS]...

You MUST authenticate with AWS using an MFA device in the shell that will execute this script

Options:

    -h    print usage and exit

Examples:
    
    \$ xargs $0<[BUCKET.LIST FILE]

EOF
}

for BUCKET in "$@"; do
    # https://jmespath.org/ for the --query option
    if [ "${BUCKET}" = "$(aws s3api list-buckets --output text --query "Buckets[?Name=='${BUCKET}'].Name")" ]; then
        log "${BUCKET} bucket exists already"
    else
        log "${BUCKET} not in S3 |#| Creating now"
        aws s3api create-bucket \
            --bucket "${BUCKET}" \
            --acl private
        # Ensure the bucket can not be given public access by anyone
        aws s3api put-public-access-block \
            --bucket "${BUCKET}" \
            --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"
        log "Bucket named ${BUCKET} has been created\n"
    fi
done

exit "$?"
