#!/bin/sh
# shellcheck disable=SC2317
# scrippies/gitlab-integration-pipeline-email

set -eu

export LC_ALL=C

this="$(realpath "$0")"
readonly this="${this}"
here="$(dirname "${this}")"
readonly here="${here}"
whatami="$(basename "${this}")"
readonly whatami="${whatami}"

cleanup() {
    status="$?"
    return "${status}"
}
trap cleanup EXIT

log() { echo "${whatami}[$$]:" "$@" >&2; }
error() { log "ERROR:" "$@"; }
warning() { log "WARNING:" "$@"; }
info() { log "INFO:" "$@"; }

die() {
    error "$@"
    usage >&2
    exit 1
}

usage() {
    cat <<EOF

Usage: $0 [OPTION]... PROJECT_URL [KEY=VAL]...
Get and/or set the given project integration pipeline-email.

Options:

    -h    print usage and exit

Examples:

    \$ $0 -h

    \$ $0 https://gitlab.com/realtime-robotics/rtr-robot-models-abb

    \$ $0 https://gitlab.com/realtime-robotics/rtr-robot-models-abb recipients=devops@rtr.ai

    \$ $0 https://gitlab.com/realtime-robotics/rtr-robot-models-abb recipients=devops@rtr.ai notify_only_broken_pipelines=true

    \$ $0 https://gitlab.com/realtime-robotics/rtr-robot-models-abb recipients=devops@rtr.ai branches_to_be_notified=default

References:

    \* https://docs.gitlab.com/ee/api/integrations.html#pipeline-emails

EOF
}

urlencode() {
    jq -sRr @uri
}

urldecode() {
    # https://stackoverflow.com/questions/28309728/decode-url-in-bash/28309957#28309957
    sed -E -e 's/[+]/ /g' -e 's/[%]([[:xdigit:]]{2})/\\\\x\1/g' \
        | xargs printf '%b\n'
}

# convert the given argument(s) to a POSIX-safe basename
posix_namify() {
    # https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap03.html#tag_03_282
    # "Portable Filename Character Set"
    printf '%s' "$*" | sed -E 's/[^[:alnum:].-]+/_/g;s/^_//;s/_$//'
}

# https://www.etalabs.net/sh_tricks.html "Working with arrays"
save_args() {
    for i; do printf %s\\n "$i" | sed "s/'/'\\\\''/g;1s/^/'/;\$s/\$/' \\\\/"; done
    echo " "
}

################################################################################
################################################################################
################################################################################

while getopts ":h" opt; do
    case "${opt}" in
        h)
            usage
            exit 0
            ;;
        :) die "missing argument: -${OPTARG}" ;;
        \?) die "bad option: -${OPTARG}" ;;
    esac
done
shift "$((OPTIND - 1))"

################################################################################

if ! [ 0 -lt "$#" ]; then
    die bad args
fi
if ! PROJECT_URL="$(echo "$1" | grep -Ex 'https://gitlab[.]com(/[^/]+)+')"; then
    die "bad PROJECT_URL:" "$1"
fi
readonly PROJECT_URL="${PROJECT_URL}"
info PROJECT_URL: "${PROJECT_URL}"

################################################################################

# Shift past the first argument.
shift 1
curl_args=""
# Any remaining positional arguments must be KEY=VAL parameters destined for a
# PUT request. For each...
for kv in "$@"; do
    # Verify it has the proper form.
    if ! echo "${kv}" | grep -Exq '[[:alnum:]_]+=[[:graph:]]+'; then
        die "bad KEY=VAL:" "${kv}"
    fi
    # This is how you append things when you store an argument array in a
    # variable.
    curl_args="$(
        eval "set -- ${curl_args-}"
        save_args "$@" --data-urlencode "${kv}"
    )"
done

################################################################################

# Parse the PROJECT_URL to assign the url-encoded PROJECT_ID.

# https://askubuntu.com/questions/53770/how-can-i-encode-and-decode-percent-encoded-strings-on-the-command-line/712206#712206
PROJECT_ID="$(echo "${PROJECT_URL}" | cut -d/ -f4- | tr -d '[:space:]' | urlencode)"
readonly PROJECT_ID="${PROJECT_ID}"
info "PROJECT_ID:" "${PROJECT_ID}"

################################################################################

export GITLAB_API_ENDPOINT="${GITLAB_API_ENDPOINT:-${CI_API_V4_URL:-https://gitlab.com/api/v4}}"
info "GITLAB_API_ENDPOINT:" "${GITLAB_API_ENDPOINT}"

################################################################################

# This is the API endpoint that we can hit with curl.
PIPELINES_EMAIL_URL="$(
    printf \
        '%s/projects/%s/integrations/pipelines-email' \
        "${GITLAB_API_ENDPOINT}" \
        "${PROJECT_ID}"
)"
info PIPELINES_EMAIL_URL: "${PIPELINES_EMAIL_URL}"

# This is the file (basename) that will store the value returned from the API
# request.
PIPELINES_EMAIL_OUT="$(
    posix_namify "$(printf '%s.json' "${PIPELINES_EMAIL_URL}" | urldecode)"
)"
info PIPELINES_EMAIL_OUT: "${PIPELINES_EMAIL_OUT}"

################################################################################

# Vet the environment variable credentials and assign the authentication
# header.
if [ -n "${GITLAB_API_PRIVATE_TOKEN-}" ]; then
    HEADER="PRIVATE-TOKEN: ${GITLAB_API_PRIVATE_TOKEN}"
elif [ -n "${CI_JOB_TOKEN-}" ]; then
    HEADER="JOB-TOKEN: ${CI_JOB_TOKEN}"
else
    die "need one, missing all:" "GITLAB_API_PRIVATE_TOKEN" "CI_JOB_TOKEN"
fi

################################################################################

curlxxx() {
    curl \
        --fail \
        --retry 3 \
        --location \
        --show-error \
        --silent \
        --header "${HEADER}" \
        --output "${PIPELINES_EMAIL_OUT}" \
        "${PIPELINES_EMAIL_URL}" \
        "$@"
}

if [ -n "${curl_args-}" ]; then
    info "putting..."
    eval "set -- ${curl_args}"
    curlxxx --request PUT "$@"
else
    info "getting..."
    curlxxx --request GET
fi

jq . "${PIPELINES_EMAIL_OUT}" >"${PIPELINES_EMAIL_OUT}".pretty
mv "${PIPELINES_EMAIL_OUT}".pretty "${PIPELINES_EMAIL_OUT}"

cat "${PIPELINES_EMAIL_OUT}"

exit "$?"
